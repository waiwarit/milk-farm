package com.me.stock.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.me.stock.errors.ProductNotFoundException;
import com.me.stock.model.Milk;
import com.me.stock.service.StockRepository;
import com.me.stock.service.StockService;

@RestController
public class StockController {
	@Autowired
	private StockService service;
	@Autowired
	private StockRepository productService;
	
	//Test in homePage
	@GetMapping("/homepage")
	public String getShopname() {
		return "Hello Milk shop";
	}
	
	//Add Milk
	@PostMapping("/stock/add")
	public ResponseEntity<Object> addProduct(@Valid @RequestBody Milk milk) {
		Milk saveMilk = service.save(milk);
		System.out.println("success");
		URI location = ServletUriComponentsBuilder // Response location and 201 create
				.fromCurrentRequest().path("/{id}").buildAndExpand(saveMilk.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	//Get all Milk
	@GetMapping("/stock")
	public List<Milk> getAllMilk(){
		return service.listAll();
	}

	@GetMapping("/stock/{id}")
	public Milk getOneMilk(@PathVariable long id) {
		Optional<Milk> milk = productService.findById(id);
		if (!milk.isPresent()) {
			throw new ProductNotFoundException("id-"+id);
		}
		return null;
	}
	
	@DeleteMapping("/stock/delete/{id}")
	public void delete(@PathVariable long id) {
		service.deleteOne(id);
	}
	
}
