package com.me.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.me.stock.model.Milk;

@Service
public class StockService {
	@Autowired
	private StockRepository repo;
	
	public List<Milk> listAll(){
		return repo.findAll();
	}
	
	public Milk save(Milk milk) {
		return repo.save(milk);
	}
	
	public Milk findbyId(long id) {
		return repo.findById(id).get();
	}
	
	public void deleteOne(long id) {
		repo.deleteById(id);
	}

}
