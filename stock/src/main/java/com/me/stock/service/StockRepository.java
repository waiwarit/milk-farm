package com.me.stock.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.me.stock.model.Milk;

@Repository
public interface StockRepository extends JpaRepository<Milk, Long>{

}
