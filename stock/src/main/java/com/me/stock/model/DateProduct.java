package com.me.stock.model;

public interface DateProduct {
	public abstract void mfg(String mfg_date); //วันผลิต
	public abstract void exp(String exp_date); //วันหมดอายุ
}
