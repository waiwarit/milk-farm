package com.me.stock.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@MappedSuperclass
abstract class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Size(min=2, message="Name should have 2 characters at least")
	private String name;
	private int number;
	private double price;
	
	public Product() {
		
	};
	
	public Product(String id) {
		
	}
	public Product(String id, String name) {
		
	};
	public Product(String id, String name, int number) {
		
	};
	public Product(String id, String name, int number, double price) {
		
	};
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	abstract void thisDetails(String details);

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", number=" + number + ", price=" + price + "]";
	}

}
