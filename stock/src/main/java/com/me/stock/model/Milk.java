package com.me.stock.model;

import javax.persistence.*;

@Entity
public class Milk extends Product implements DateProduct{
	private String details;
	private String mfg_date;
	private String exp_date;

	@Override
	void thisDetails(String details) {
		// TODO Auto-generated method stub
		this.details = details;
	}

	@Override
	public void mfg(String mfg_date) {
		// TODO Auto-generated method stub
		this.mfg_date = mfg_date;
	}

	@Override
	public void exp(String exp_date) {
		// TODO Auto-generated method stub
		this.exp_date = exp_date;
	}
	
	public String get_mfg() {
		return this.mfg_date;
	}
	
	public String get_exp() {
		return this.exp_date;
	}
	
	public String get_datails() {
		return this.details;
	}

	@Override
	public String toString() {
		return "Milk [details=" + details + ", mfg_date=" + mfg_date + ", exp_date=" + exp_date + ", get_mfg()="
				+ get_mfg() + ", get_exp()=" + get_exp() + ", get_datails()=" + get_datails() + ", getId()=" + getId()
				+ ", getName()=" + getName() + ", getNumber()=" + getNumber() + ", getPrice()=" + getPrice()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}
	
}
